import os
from flask import Flask, redirect, url_for, request, render_template, jsonify, session
from pymongo import MongoClient
import acp_times
import config

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.times
collection = db.controls


@app.route('/')
@app.route('/index')
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')


@app.route("/new", methods=["POST"])
def new():
    """
    Add our control times/distances to the database.
    """
    # Clear the collection in the database
    collection.drop()
    # Initialize a list for the request.form data to be put in
    content = []
    app.logger.debug("request form: {}".format(request.form))
    # Loop through the request sent from the server and add data to content list
    for item in request.form:
        item_list = request.form.getlist(item)
        content.append(item_list)
    app.logger.debug("content: {}".format(content))
    # Loop through data from every row in the table on client side (20 rows)
    for i in range(20):
        # Create a dict of the items to insert into the database
        item_doc = {
            'distance': content[0][0],
            'begin_date': content[1][0],
            'begin_time': content[2][0],
            'miles': content[3][i],
            'km': content[4][i],
            'location': content[5][i],
            'open': content[6][i],
            'close': content[7][i]
        }
        # Don't insert empty controls into the database
        if item_doc['miles'] != "":
            collection.insert_one(item_doc)
    # Reset the page
    return redirect(url_for('index'))


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    # Get the arguments from the client
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('dist', 999, type=float)
    datetime = request.args.get('datetime')

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # Calculate open and close times using arguments from the client
    open_time = acp_times.open_time(km, distance, datetime)
    close_time = acp_times.close_time(km, distance, datetime)
    result = {"open": open_time, "close": close_time}

    # Send data back to the client
    return jsonify(result=result)


@app.route('/display')
def display():
    _items = collection.find()
    items = [item for item in _items]
    length = len(items)
    app.logger.debug("items: {}".format(items))
    return render_template('display.html', items=items, length=length)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
